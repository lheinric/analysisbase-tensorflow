FROM atlas/analysisbase
USER root
RUN source /home/atlas/release_setup.sh && \
    pip install -U argparse   && \
    pip install tensorflow
    
USER atlas